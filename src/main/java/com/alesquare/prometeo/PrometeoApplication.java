package com.alesquare.prometeo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing(auditorAwareRef = "customAuditorAware")
public class PrometeoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrometeoApplication.class, args);
    }

}
