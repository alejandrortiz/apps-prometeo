package com.alesquare.prometeo.controller;

import com.alesquare.prometeo.model.User;
import com.alesquare.prometeo.service.interfaces.IUserService;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api/users")
public class UserController {

    private final IUserService userService;

    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<?> findAll() {
        List<User> users = null;

        try {
            users = this.userService.findAll();
        } catch (DataAccessException e) {
            String message = "Error when consulting the database";
            String error = Objects.requireNonNull(e.getMessage()).concat(": ").concat(e.getMostSpecificCause().getMessage());

            handlerResponseException(HttpStatus.INTERNAL_SERVER_ERROR, message, error);
        }

        Map<String, Object> response = new HashMap<>();
        response.put("status", HttpStatus.OK.value());
        response.put("message", "Users found");
        response.put("data", users);
        response.put("count", Objects.requireNonNull(users).size());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable UUID id) {
        User user;

        try {
            user = this.userService.findById(id);
        } catch (DataAccessException e) {
            String message = "Error when consulting the database";
            String error = Objects.requireNonNull(e.getMessage()).concat(": ").concat(e.getMostSpecificCause().getMessage());

            return handlerResponseException(HttpStatus.INTERNAL_SERVER_ERROR, message, error);
        }

        if (null == user) {
            String message = "User ".concat(id.toString()).concat(" does not exist.");

            return handlerResponseException(HttpStatus.NOT_FOUND, message, null);
        }

        return handlerResponse(HttpStatus.OK, "User found", user);
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody User user) {
        User userCreated;

        try {
            userCreated = this.userService.save(user);
        } catch (DataAccessException e) {
            String message = "Error when trying to insert data into the database";
            String error = Objects.requireNonNull(e.getMessage()).concat(": ").concat(e.getMostSpecificCause().getMessage());

            return handlerResponseException(HttpStatus.INTERNAL_SERVER_ERROR, message, error);
        } catch (Exception e) {
            String message = "Error when trying to insert data into the database";
            String error = e.getMessage();

            return handlerResponseException(HttpStatus.INTERNAL_SERVER_ERROR, message, error);
        }

        return handlerResponse(HttpStatus.CREATED, "User created", userCreated);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody User user, @PathVariable UUID id) {
        User userUpdated;

        User currentUser = this.userService.findById(id);

        if (null == currentUser) {
            String message = "Cannot be updated because user ".concat(id.toString()).concat(" does not exist.");

            return handlerResponseException(HttpStatus.NOT_FOUND, message, null);
        }

        try {
            currentUser.setName(user.getName());
            currentUser.setLastName(user.getLastName());
            currentUser.setUsername(user.getUsername());
            currentUser.setMail(user.getMail());
            currentUser.setPassword(user.getPassword());
            currentUser.setAvatar(user.getAvatar());

            userUpdated = this.userService.save(currentUser);
        } catch (DataAccessException e) {
            String message = "Error when trying to insert data into the database";
            String error = Objects.requireNonNull(e.getMessage()).concat(": ").concat(e.getMostSpecificCause().getMessage());

            return handlerResponseException(HttpStatus.INTERNAL_SERVER_ERROR, message, error);
        }

        return handlerResponse(HttpStatus.CREATED, "User updated", userUpdated);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable UUID id) {
        try {
            this.userService.deleteById(id);
        } catch (DataAccessException e) {
            String message = "Error when trying to delete data into the database";
            String error = Objects.requireNonNull(e.getMessage()).concat(": ").concat(e.getMostSpecificCause().getMessage());

            return handlerResponseException(HttpStatus.INTERNAL_SERVER_ERROR, message, error);
        }

        return handlerResponse(HttpStatus.OK, "User deleted", null);
    }

    private ResponseEntity<?> handlerResponseException(HttpStatus status, String message, Object error) {
        Map<String, Object> response = new HashMap<>();
        response.put("status", status.value());
        response.put("message", message);

        if (null != error) {
            response.put("error", error);
        }

        return new ResponseEntity<>(response, status);
    }

    private ResponseEntity<?> handlerResponse(HttpStatus status, String message, Object data) {
        Map<String, Object> response = new HashMap<>();
        response.put("status", status.value());
        response.put("message", message);

        if (null != data) {
            response.put("data", data);
        }

        return new ResponseEntity<>(response, status);
    }
}
