package com.alesquare.prometeo.service.interfaces;

import com.alesquare.prometeo.model.User;

import java.util.List;
import java.util.UUID;

public interface IUserService {
    public List<User> findAll();

    public User findById(UUID id);

    public User save(User user);

    public void deleteById(UUID id);
}
