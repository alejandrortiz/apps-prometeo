package com.alesquare.prometeo.repository;

import com.alesquare.prometeo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface IUserRepository extends JpaRepository<User, UUID> {
}
