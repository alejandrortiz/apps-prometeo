package com.alesquare.prometeo.model.audit;

import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CustomAuditorAware implements AuditorAware {
    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of("test");
    }
}
