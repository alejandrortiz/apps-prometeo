package com.alesquare.prometeo.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class StrongPasswordValidator implements ConstraintValidator<StrongPassword, String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        return value.matches("((?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%!]).{8,40})");
    }
}
